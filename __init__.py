# -*- coding: utf-8 -*-

'''Convert string to and from base 64.'''

import os
import base64

from albert import *

md_iid = '0.5'
md_version = '1.0'
md_name = 'Base64 converter'
md_description = 'Base64 encode/decode strings.'
__doc__ = 'Given a string this plugin encodes and decodes from/to base 64. The action copies the result to clipboard.'
md_license = 'MIT'
md_url = 'https://gitlab.com/emilioidk/albert-base64'
md_maintainers = 'Emilio Martin Lundgaard Lopez'
md_lib_dependencies = ['base64']

extensionPath = os.path.dirname(__file__)
iconPathEncoded = '{}/encode.gif'.format(extensionPath)
iconPathDecoded = '{}/decode.gif'.format(extensionPath)


class Plugin(QueryHandler):
    def id(self):
        return 'Base64Converter'

    def name(self):
        return md_name

    def description(self):
        return __doc__

    def defaultTrigger(self):
        return 'base64 '

    def initialize(self):
        debug('Initialized Base64 Converter')

    def finalize(self):
        debug('Finalized Base64 Converter')

    def synopsis(self):
        return 'base64 <string>'

    def handleQuery(self, query):
        if query.isValid and len(query.string) > 0:
            items = []

            # Encode
            base64encoded = base64.b64encode(query.string.encode('ascii'))
            items.append(
                Item(
                    id='base64encoded',
                    text=base64encoded,
                    subtext='Encoded to base 64',
                    completion='',
                    icon=[iconPathEncoded],
                    actions=[
                        Action(
                            id='encode_copy',
                            text='Copy to Clipboard',
                            callable=lambda: setClipboardText(text=base64encoded)
                        )
                    ]
                )
            )

            # Decode
            try:
                base64decoded = base64.b64decode(query.string)
                items.append(
                    Item(
                        id='base64decoded',
                        text=base64decoded,
                        subtext='Decoded from base 64',
                        icon=[iconPathDecoded],
                        actions=[
                            Action(
                                id='decode_copy',
                                text='Copy to Clipboard',
                                callable=lambda: setClipboardText(text=base64decoded)
                            )
                        ]
                    )
                )
            except:
                pass

            query.add(items)
